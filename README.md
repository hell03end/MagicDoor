# MAGIC-DOOR-8-9.10.17
Repo for hackaton AR app (24- hours :) )

Portal door from real world to virtual space (for watching only, not for stepping into).

### Requirements
* Unity 2017.1.1f1
* Vuforia 6.5
* Android SDK 23+
* JDK 1.8+

### Example
Model demonstration for sample image trigger.

![gif](https://gitlab.com/hell03end/MagicDoor/raw/master/res/DEMOAR.gif)

### Build
Clone repo:
```bash
git clone https://github.com/hell03end/MagicDoor.git
cd MagicDoor
```

Open Unity and build project for correct Android SDK version.

### License
[MIT](https://github.com/hell03end/MagicDoor/blob/master/LICENSE)

### Contributors
* [Alexey Ivanov](https://github.com/DiggiDon) @DiggiDon
* [Max Mazhanov](https://github.com/VoiceProj) @VoiceProj
* [Danil Sosinsky](https://github.com/sosdan73) @sosdan73
